#include <QGuiApplication>
#include <QDebug>

#include <KSharedConfig>
#include <KConfigGroup>
#include <KIO/ApplicationLauncherJob>
#include <KService>

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);
    a.setDesktopSettingsAware(false);
    KSharedConfig::Ptr config = KSharedConfig::openConfig("davessessionrestore", KConfig::NoGlobals);

    QList<KJob *> jobs;
    QEventLoop e;

    for (const QString &groupName : config->groupList()) {
        const QString appId = config->group(groupName).readEntry("appId");
        auto service = KService::serviceByDesktopName(appId);
        if (!service || !service->isValid()) {
            qDebug() << "skipping " << appId << "no service found.";
            continue;
        }

        if (service->noDisplay()) {
            continue;
        }

        qDebug() << "launching " << service->name();
        auto job = new KIO::ApplicationLauncherJob(service);

        QObject::connect(job, &KJob::finished, &e, [job, &jobs, &e]() {
            jobs.removeOne(job);
            if (jobs.isEmpty()) {
                e.quit();
            }
        });
        jobs << job;
        job->start();
    }
    if (!jobs.isEmpty()) {
        e.exec();
    }

    return 0;
}
