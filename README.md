# Session Restore app

This repo consists of two binaries:

One that takes a list of all running applications as reported by the PlasmaWindowManagement protocol and associated appIds and saves it to a config file.

One that reads that config file and restores all the relevant apps

# There is no system integration (yet)
